// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (C) 2019 Information Sciences Institute. All Rights Reserved.
 */

#include <linux/skbuff.h>
#include <linux/netdevice.h>

bool lb_do_receive(struct sk_buff **skbp)
{
  struct net_device *lb_dev;
  struct sk_buff *skb = *skbp; 

  lb_dev = __dev_get_by_name(&init_net, "lb");
  if (!lb_dev) {
    printk(KERN_WARNING "lb device not found");
  }

  skb->dev = lb_dev;

  //accept all the things
  return true;
}
