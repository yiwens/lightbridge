// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (C) 2019 Information Sciences Institute. All Rights Reserved.
 */

#include "netlink.h"
#include "uapi/lightbridge.h"

#include <linux/if.h>
#include <net/genetlink.h>

static const struct nla_policy device_policy[LBDEVICE_A_MAX + 1] = {
	[LBDEVICE_A_IFINDEX] = { .type = NLA_U32 },
	[LBDEVICE_A_IFNAME]  = { .type = NLA_NUL_STRING, .len = IFNAMSIZ - 1 },
	[LBDEVICE_A_CIRCUIT] = { .type = NLA_U32 },
	[LBDEVICE_A_PARENT]  = { .type = NLA_U32 }
};

static struct genl_family genl_family = {
	.id = 0,
	.hdrsize = 0,
	.name = "lightbridge",
	.version = 1,
	.maxattr = LBDEVICE_A_MAX,
};

static int lb_get_device_start(struct netlink_callback *cb) 
{
	//TODO
	return 0;
}

static int lb_get_device_dump(struct sk_buff *skb, struct netlink_callback *cb)
{
	//TODO
	return 0;
}

static int lb_get_device_done(struct netlink_callback *cb)
{
	//TODO
	return 0;
}

static int lb_set_device(struct sk_buff *skb, struct genl_info *info)
{
	//TODO
	return 0;
}

static const struct genl_ops genl_ops[] = {
	{
		.cmd = LB_CMD_GET_DEVICE,
		.start = lb_get_device_start,
		.dumpit = lb_get_device_dump,
		.done = lb_get_device_done,
		.policy = device_policy,
		.flags = GENL_UNS_ADMIN_PERM
	},
	{
		.cmd = LB_CMD_SET_DEVICE,
		.doit = lb_set_device,
		.policy = device_policy,
		.flags = GENL_UNS_ADMIN_PERM,
	}
};

int __init lb_genetlink_init(void)
{
	return genl_register_family(&genl_family);
}

void __exit lb_genetlink_uninit(void) 
{
	genl_unregister_family(&genl_family);
}
