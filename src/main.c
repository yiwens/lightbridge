// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (C) 2019 Information Sciences Institute. All Rights Reserved.
 */

#include "netlink.h"
#include "uapi/lightbridge.h"

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/init.h>
#include <linux/moduleparam.h>
#include <linux/rtnetlink.h>
#include <linux/net_tstamp.h>
#include <net/rtnetlink.h>
#include <linux/genetlink.h>
#include <linux/u64_stats_sync.h>
#include <linux/slab.h>
#include <linux/if_arp.h>
#include <net/arp.h>

#define DRV_NAME    "lightbridge"
#define DRV_VERSION "1.0"
#define DRIVER_DESC  "Lightbridge: communicate over optically switched networks"

struct net_device *lightbridge_dev;

// private data struct used by this interface for stats, transmit data, etc
struct lightbridge_private {
	struct net_device_stats priv_stats;
	struct net_device *priv_device;		// interface used to xmit data
};

// Operations ----------------------------------------------------------------
// open and close 
int lightbridge_open(struct net_device *dev)
{
	struct lightbridge_private *priv = netdev_priv(lightbridge_dev);

	// Mark device as operational
	printk(KERN_INFO "%s device opened\n", dev->name);
	netif_start_queue(dev);

  if (netif_carrier_ok(priv->priv_device)) {
    netif_carrier_on(dev);
  }
	return 0;
}


int lightbridge_close(struct net_device *dev)
{
	// Mark device as closed
	printk(KERN_INFO "%s device closed\n", dev->name);
	netif_stop_queue(dev);
	return 0;
}

static int lightbridge_create_header(struct sk_buff *skb, struct net_device *dev, 
		unsigned short type, const void *daddr, const void *saddr, unsigned len)
{
	struct lightbridge_private *priv = netdev_priv(lightbridge_dev);
	int retval;

	skb->dev = priv->priv_device;
	retval = skb->dev->header_ops->create(skb, skb->dev, type, daddr, saddr, len);
	skb->dev = dev;
	return retval;
}

static int lb_dev_neigh_setup(struct net_device *dev, struct neigh_parms *pa)
{
	struct net_device *real_dev = lb_dev_priv(dev)->priv_device;
	const struct net_device_ops *ops = real_dev->netdev_ops;
	int err = 0;

	if (netif_device_present(real_dev) && ops->ndo_neigh_setup)
		err = ops->ndo_neigh_setup(real_dev, pa);

	return err;
}

// get stats
struct net_device_stats *lightbridge_get_stats(struct net_device *dev)
{
	return &((struct lightbridge_private *)netdev_priv(dev))->priv_stats;
}

// transmit packets
int lightbridge_xmit(struct sk_buff *skb, struct net_device *dev)
{
	struct lightbridge_private *priv = netdev_priv(dev);

	printk(KERN_INFO "At lightbridge_xmit");

	// if cannot send to anyone
	if(!priv->priv_device) {	
		kfree_skb(skb);
		priv->priv_stats.tx_errors++;
		priv->priv_stats.tx_dropped++;
		
		return 0;
	}
	
	// otherwise update its own stats counters
	priv->priv_stats.tx_packets++;
	priv->priv_stats.tx_bytes += skb->len;

	// assign packet to real hardware interface
	skb->dev = priv->priv_device;
	skb->priority = 1;

	// tell Linux to pass it to its device
	dev_queue_xmit(skb);

	return 0;
}

// Packet reception

static void lb_dev_set_rx_mode(struct net_device *lb_dev)
{
  dev_mc_sync(lb_dev_priv(lb_dev)->priv_device, lb_dev);
  dev_uc_sync(lb_dev_priv(lb_dev)->priv_device, lb_dev);
}

static void lb_dev_change_rx_flags(struct net_device *dev, int change)
{
	struct net_device *real_dev = lb_dev_priv(dev)->priv_device;

	if (dev->flags & IFF_UP) {
		if (change & IFF_ALLMULTI)
			dev_set_allmulti(real_dev, dev->flags & IFF_ALLMULTI ? 1 : -1);
		if (change & IFF_PROMISC)
			dev_set_promiscuity(real_dev, dev->flags & IFF_PROMISC ? 1 : -1);
	}
}

// header_ops
static const struct header_ops lightbridge_header_ops = {
	.create 	= lightbridge_create_header,
	.cache  	= NULL,
};


// netdev_ops struct
static const struct net_device_ops lightbridge_net_device_ops = {
	.ndo_open 		= lightbridge_open,
	.ndo_stop		= lightbridge_close,
	.ndo_get_stats		= lightbridge_get_stats,
	.ndo_set_mac_address	= eth_mac_addr,
	.ndo_start_xmit		= lightbridge_xmit,
  	.ndo_set_rx_mode 	= lb_dev_set_rx_mode,
  	.ndo_change_rx_flags 	= lb_dev_change_rx_flags,
	.ndo_neigh_setup	= lb_dev_neigh_setup,
};

// initialization -------------------------------------------------------------
void lightbridge_init(struct net_device *dev)
{
	struct net_device *parent;
	struct lightbridge_private *priv = lb_dev_priv(dev);

	printk(KERN_INFO "At lightbridge_init ---------------------------------");

	ether_setup(dev);	// fill fields of dev struct with generic Ethernet values
	memset(lb_dev_priv(dev), 0, sizeof(struct lightbridge_private));

	dev->netdev_ops = &lightbridge_net_device_ops;
	
	// connect to parent interface enp3s0
	parent = __dev_get_by_name(&init_net, "eth1");
	if (!parent) {
		printk(KERN_WARNING "No parent\n");
	}
	printk(KERN_INFO "Name of parent interface: %s\n", parent->name);
	
	priv->priv_device = parent;	// set physical hardware device
	
	// build headers 
	if (parent->header_ops) {
		dev->header_ops = &lightbridge_header_ops;
	}
	else {
		dev->header_ops = NULL;
	}

	// copy and clone parent interface's IP, MAC and other info
	ether_addr_copy(dev->dev_addr, parent->dev_addr);

}


// module entry point
int __init lightbridge_init_module(void)
{
	
	int err;
	//XXX netlink registration instead
	lightbridge_dev = alloc_netdev(sizeof(struct lightbridge_private),
					"lb", NET_NAME_UNKNOWN, lightbridge_init);
	
	if(!lightbridge_dev)
		return -ENOMEM;

	err = dev_alloc_name(lightbridge_dev, lightbridge_dev->name);
	if (err < 0) {
		printk(KERN_WARNING "%s allocate name error %i\n", lightbridge_dev->name, err);
		return -EIO;
	}
	
	err = register_netdev(lightbridge_dev);
	if (err < 0) {
		printk(KERN_WARNING "%s registration error %i\n", lightbridge_dev->name, err);
		return -EIO;
	}
	//end XXX
	
	err = lb_genetlink_init();
	if(err < 0) 
		return err;
	
	printk(KERN_INFO "%s device registered\n", lightbridge_dev->name);
	return 0;
}

void __exit lightbridge_exit_module(void)
{
	unregister_netdev(lightbridge_dev);
	printk("%s device unregistered\n", lightbridge_dev->name);
	free_netdev(lightbridge_dev);
}

module_init(lightbridge_init_module);
module_exit(lightbridge_exit_module);

MODULE_LICENSE("GPL v2");
MODULE_AUTHOR("Yiwen Shen <ys2799@columbia.edu>");
MODULE_AUTHOR("Ryan Goodfellow <rgoodfel@isi.edu>");
MODULE_DESCRIPTION(DRIVER_DESC);
MODULE_VERSION(DRV_VERSION);
MODULE_ALIAS_RTNL_LINK(KBUILD_MODNAME);
MODULE_ALIAS_GENL_FAMILY(LB_GENL_NAME);
