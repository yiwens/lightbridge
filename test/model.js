topo = {
  name: 'lightbridge',
  nodes: [deb('a'), deb('b')],
  links: [Link('a', 1, 'b', 1)],
}

function deb(name) {
  return {
    name: name,
    image: 'debian-buster',
    cpu: { cores: 8 },
    memory: { capacity: GB(8) },
    mounts: [{ source: env.PWD+'/..', point: '/tmp/lightbridge'}]
  }
}
