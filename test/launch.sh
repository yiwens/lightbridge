#!/bin/bash

BLUE="\e[34m"
NORMAL="\e[39m"
blue() { 
  echo -e "$BLUE$1$NORMAL" 
}

set -e

blue "clearing out any existing state"
rvn destroy

blue "building"
rvn build

blue "deploying"
rvn deploy

blue "waiting for deployment to become ready"
rvn pingwait a b

blue "configuring"
rvn configure
rvn status

blue "running setup"
ansible-playbook -i .rvn/ansible-hosts setup.yml
