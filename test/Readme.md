# Lightbridge testing environment

## Prerequisites

Requires [raven](https://gitlab.com/rygoo/raven).

## Setup

```shell
# must be root
sudo su

# spin up the testing environment
./launch.sh
```

## Building, installing and enabling lightbridge

```shell
# jump into testing VM
eval $(rvn ssh a)

# go to mounted lightbridge source directory, build and install
sudo su
cd /tmp/lightbridge/linux
cp ../test/kconfig .config
make oldconfig
make -j`nproc`
make install
make modules_install
depmod -a

# insert the kernel module
modprobe lightbridge
```


